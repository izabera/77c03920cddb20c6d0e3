#!/bin/sh
abspath() {
  path=$1/

  case $path in
    /*) :;;
    *) path=$PWD/$path;;
  esac

  while :; do
    case $path in

      # /foo/./bar               -> /foo/bar
      */./*)
        pre=${path%%/./*}
        post=${path#*/./}
        path=$pre/$post ;;

      # /foo//bar                -> /foo/bar
      *//*)
        pre=${path%%//*}
        post=${path#*//}
        path=$pre/$post ;;

      # /../foo                  -> /foo
      /../*)
        path=${path#/..} ;;

      # /foo/bar/../baz          -> /foo/baz
      */../*)
        pre=${path%%/../*}
        pre=${pre%/*}
        post=${path#"$pre"/*/..}
        path=$pre/$post ;;

      *) break ;;
    esac

  done

  if [ "$path" != / ]; then path=${path%/}; fi
  printf %s\\n "$path"
}


abspath a/b//c/./d/../e